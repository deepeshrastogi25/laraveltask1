<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/register');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['App\Http\Middleware\UserMiddleware'], 'prefix' => 'user', 'as' => 'user.'], function () {
    Route::get('/', 'UserController@index')->name('home');
});

Route::group(['middleware' => ['App\Http\Middleware\AdminMiddleware'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/', 'AdminController@index')->name('home');
    Route::post('/getUser', 'AdminController@userList')->name('user');
});