<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use DB;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loginUser = Auth::user();
        $expected_income = $loginUser->expected_income;
        $preferred_occupation = $loginUser->preferred_occupation;
        $preferred_family_type = $loginUser->preferred_family_type;
        $preferred_manglik = $loginUser->preferred_manglik;
        $gender = $loginUser->gender;
        $user_id = $loginUser->id;
        // $preferredUserList = User::where([["id","<>",$user_id],["gender","<>",$gender],["annual_income",">=",$expected_income],"occupation"=>$preferred_occupation,"manglik"=>$preferred_manglik,"family_type"=>$preferred_family_type])->get()->toArray();
        $total_count = $annual_income_match_count = $occupation_match_count = $manglik_match_count = $preferred_family_type = 0;
        
        $select[]="*";

        ##################### annual_incomeannual_income ###################
        $annual_income_match_data = DB::Raw("annual_income >= ".$expected_income);
        // $select['annual_income_match_data'] = $annual_income_match_data;
        $annual_income_match_count = DB::Raw("IF(($annual_income_match_data)>0,'1','0')");

        $select['annual_income_match_count'] = DB::Raw($annual_income_match_count." as annual_income_match_count");
        $total_count++;
        ##################### annual_incomeannual_income ###################
        
        ##################### occupation ###################
        $occupation_match_data = DB::Raw("occupation = ".$preferred_occupation);
        $occupation_match_count = DB::Raw("IF(($occupation_match_data)>0,'1','0')");

        $select['occupation_match_count'] =  DB::Raw($occupation_match_count." as occupation_match_count");
        $total_count++;
        ##################### occupation ###################

        ##################### manglik ###################
        $manglik_match_data = DB::Raw("manglik = ".$preferred_manglik);
        $manglik_match_count = DB::Raw("IF(($manglik_match_data)>0,'1','0')");

        $select['manglik_match_count'] =  DB::Raw($manglik_match_count." as manglik_match_count");
        $total_count++;
        ##################### manglik ###################

         ##################### family_type ###################
         $preferred_family_type = DB::Raw("family_type = ".$preferred_family_type);
         $family_type_match_count = DB::Raw("IF(($preferred_family_type)>0,'1','0')");
 
         $select['family_type_match_count'] =  DB::Raw($family_type_match_count." as family_type_match_count");
         $total_count++;
         ##################### family_type ###################

        $select["matching_percent"] =  DB::Raw("
            (
             ".$annual_income_match_count."+ ".$occupation_match_count."+ ".$manglik_match_count."+ ".$family_type_match_count."
            ) * 100 / ".$total_count." as matching_percent");
        $select["total_counter"] =  DB::Raw($total_count." as total_counter");

        $preferredUserList = User::select($select)->where([["id","<>",$user_id],["gender","<>",$gender],"user_type"=>"1"])->orderBy("matching_percent","DESC")->get();
        $data["preferredUserList"] = $preferredUserList;
        $data["occupations"] = config('global.occupations');
        $data["familyType"] = config('global.familyType');
        $data["manglikType"] = config('global.manglikType');
        $data["genders"] = config('global.genders');
        $data["loginUser"] = $loginUser;
        return view('home',$data);
    }
}
