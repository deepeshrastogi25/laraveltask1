<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\User;
use Auth;
use DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        return view('admin_home',$data);
    }
    public function userList(Request $request){
        $requestData = $request->all();
        $userList = User::where(["user_type"=>"1"]);
        if(!empty($requestData)){
            $gender = $requestData["gender"];
            $family_type = $requestData["family_type"];
            $manglik = $requestData["manglik"];
            $occupation = $requestData["occupation"];
            if(!empty($gender)){
                $userList = $userList->where(["gender"=>$gender]);
            }
            if(!empty($family_type)){
                $userList = $userList->where(["family_type"=>$family_type]);
            }
            if(!empty($manglik)){
                $userList = $userList->where(["manglik"=>$manglik]);
            }
            if(!empty($occupation)){
                $userList = $userList->where(["occupation"=>$occupation]);
            }

        }
        $userList = $userList->orderBy("id","DESC")->get()->toArray();
        return response()->json(["userList" => $userList]);
    }
}
