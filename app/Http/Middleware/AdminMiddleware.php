<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->user_type == 2) {
            return $next($request);    
        }
        // return new Response(view('unauthorized'), 403);
        return new Response(view('unauthorized')->with('role', 'ADMIN'));
    }
}
