<?php

namespace App;
use App\User;

class Admin extends User
{
    protected $table = "users";
    protected $primaryKey = 'id';
}
