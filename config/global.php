<?php
   
return [
    'genders' => ["1"=>"Male","2"=>"Female"],
    'occupations' => ["1"=>"Private job","2"=>"Government Job","3"=>"Business"],
    'familyType' => ["1"=>"Joint family","2"=>"Nuclear family"],
    'manglikType' => ["1"=>"Yes","2"=>"No"]
]
  
?>