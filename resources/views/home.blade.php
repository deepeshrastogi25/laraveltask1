@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        Login User Details -
                    <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Expected Annual Income</th>
                            <th>Preferred Occupation</th>
                            <th>Preferred Family type</th>
                            <th>Preferred Manglik</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$loginUser->first_name.' '.$loginUser->last_name}}</td>
                            <td>{{$loginUser->email}}</td>
                            <td>{{$genders[$loginUser->gender]}}</td>
                            <td>{{$loginUser->expected_income}}</td>
                            <td>{{$occupations[$loginUser->preferred_occupation]}}</td>
                            <td>{{$familyType[$loginUser->preferred_family_type]}}</td>
                            <td>{{$manglikType[$loginUser->preferred_manglik]}}</td>
                        </tr>
                    </tbody>
                    </table>

            <div class="card">
                <div class="card-header">Matching List</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email </th>
                            <th>Date of birth</th>
                            <th>Gender</th>
                            <th>Annual income</th>
                            <th>Occupation</th>
                            <th>Family type</th>
                            <th>Manglik</th>
                            <th>Matching Percentage</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(sizeof($preferredUserList) > 0)
                            @foreach($preferredUserList as $preferUser)
                                <tr>
                                    <td>{{$preferUser->first_name.' '.$preferUser->last_name}}</td>
                                    <td>{{$preferUser->email}}</td>
                                    <td>{!! date('d/M/Y', strtotime($preferUser->dob)) !!}</td>
                                    <td>{{$genders[$preferUser->gender]}}</td>
                                    <td>{{$preferUser->annual_income}}</td>
                                    <td>{{$occupations[$preferUser->occupation]}}</td>
                                    <td>{{$familyType[$preferUser->family_type]}}</td>
                                    <td>{{$manglikType[$preferUser->manglik]}}</td>
                                    <td>{{$preferUser->matching_percent}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr><td colspan="9"><strong>Sorry, No records were found.</strong></td></tr>
                        @endif($)
                    </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
