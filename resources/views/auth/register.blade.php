@extends('layouts.app')

@section('content')
<div class="container">
<form method="POST" action="{{ route('register') }}">
                        @csrf
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Basic Information Section</div>

                <div class="card-body">
                   

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">First name *</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}"  autocomplete="first_name" autofocus>

                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Last name *</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}"  autocomplete="last_name" autofocus>

                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email *</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password *</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Date of birth *</label>

                            <div class="col-md-6">
                                <input id="dob" type="date" class="form-control @error('dob') is-invalid @enderror" name="dob" value="{{ old('dob') }}"  autocomplete="dob" autofocus>

                                @error('dob')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Gender *</label>

                            <div class="col-md-6">
                            @php 
                                $genders = config('global.genders');
                            @endphp
                            @foreach($genders as $key=>$gender)
                            @php $select = ""; @endphp
                            @if(!empty(old('gender')) && old('gender') == $key )
                                @php $select = "checked"; @endphp
                            @endif
                            <input id="gender" type="radio" class="@error('gender') is-invalid @enderror" name="gender" value="{{$key}}" {{$select}}> {{$gender}}
                           @endforeach
                          
                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="annual_income" class="col-md-4 col-form-label text-md-right">Annual income *</label>

                            <div class="col-md-6">
                                <input id="annual_income" type="text" class="form-control @error('annual_income') is-invalid @enderror" name="annual_income" value="{{ old('annual_income') }}"  autocomplete="annual_income" autofocus>

                                @error('annual_income')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="occupation" class="col-md-4 col-form-label text-md-right">Occupation</label>

                            <div class="col-md-6">
                                <select id="occupation" class="form-control @error('occupation') is-invalid @enderror" name="occupation" autofocus>
                                <option value="">Please select an option</option>
                                @php 
                                    $occupations = config('global.occupations');
                                @endphp
                                @foreach($occupations as $key=>$occupation)
                                    @php $select = ""; @endphp
                                    @if(!empty(old('occupation')) && old('occupation') == $key )
                                        @php $select = "selected"; @endphp
                                    @endif
                                    <option value="{{$key}}" {{$select}}>{{$occupation}}</option>
                                    @endforeach
                                </select>
                                @error('occupation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="family_type" class="col-md-4 col-form-label text-md-right">Family type</label>

                            <div class="col-md-6">
                                <select id="family_type" class="form-control @error('family_type') is-invalid @enderror" name="family_type" autofocus>
                                <option value="">Please select an option</option>
                                @php 
                                    $familyType = config('global.familyType');
                                @endphp
                                @foreach($familyType as $key=>$family)
                                    @php $select = ""; @endphp
                                    @if(!empty(old('family_type')) && old('family_type') == $key )
                                        @php $select = "selected"; @endphp
                                    @endif
                                    <option value="{{$key}}" {{$select}}>{{$family}}</option>
                                @endforeach
                                </select>
                                @error('family_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="manglik" class="col-md-4 col-form-label text-md-right">Manglik</label>

                            <div class="col-md-6">
                                <select id="manglik" class="form-control @error('manglik') is-invalid @enderror" name="manglik" autofocus>
                                <option value="">Please select an option</option>
                                @php 
                                    $manglikType = config('global.manglikType');
                                @endphp
                                @foreach($manglikType as $key=>$manglik_type)
                                    @php $select = ""; @endphp
                                    @if(!empty(old('manglik')) && old('manglik') == $key )
                                        @php $select = "selected"; @endphp
                                    @endif
                                    <option value="{{$key}}" {{$select}}>{{$manglik_type}}</option>
                                @endforeach
                                </select>
                                @error('manglik')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                       

                      
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Partner Preference</div>

                <div class="card-body">
                <div id="slider"></div>
                        <div class="form-group row">
                            <label for="expected_income" class="col-md-4 col-form-label text-md-right">Expected income *</label>

                            <div class="col-md-6">
                                <input id="expected_income" type="range" class="form-control @error('expected_income') is-invalid @enderror" name="expected_income" value="{{ old('expected_income') }}"  autocomplete="expected_income" autofocus>

                                @error('expected_income')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="preferred_occupation" class="col-md-4 col-form-label text-md-right">Occupation *</label>

                            <div class="col-md-6">
                                <select id="preferred_occupation" class="form-control @error('preferred_occupation') is-invalid @enderror" name="preferred_occupation" autofocus>
                                <option value="">Please select an option</option>
                                @php 
                                    $occupations = config('global.occupations');
                                @endphp
                                @foreach($occupations as $key=>$occupation)
                                    @php $select = ""; @endphp
                                    @if(!empty(old('preferred_occupation')) && old('preferred_occupation') == $key )
                                        @php $select = "selected"; @endphp
                                    @endif
                                    <option value="{{$key}}" {{$select}}>{{$occupation}}</option>
                                    @endforeach
                                </select>
                                @error('preferred_occupation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="preferred_family_type" class="col-md-4 col-form-label text-md-right">Family type *</label>

                            <div class="col-md-6">
                                <select id="preferred_family_type" class="form-control @error('preferred_family_type') is-invalid @enderror" name="preferred_family_type" autofocus>
                                <option value="">Please select an option</option>
                                @php 
                                    $familyType = config('global.familyType');
                                @endphp
                                @foreach($familyType as $key=>$family)
                                    @php $select = ""; @endphp
                                    @if(!empty(old('preferred_family_type')) && old('preferred_family_type') == $key )
                                        @php $select = "selected"; @endphp
                                    @endif
                                    <option value="{{$key}}" {{$select}}>{{$family}}</option>
                                @endforeach
                                </select>
                                @error('preferred_family_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="preferred_manglik" class="col-md-4 col-form-label text-md-right">Manglik *</label>

                            <div class="col-md-6">
                                <select id="preferred_manglik" class="form-control @error('preferred_manglik') is-invalid @enderror" name="preferred_manglik" autofocus>
                                <option value="">Please select an option</option>
                                @php 
                                    $manglikType = config('global.manglikType');
                                @endphp
                                @foreach($manglikType as $key=>$manglik_type)
                                    @php $select = ""; @endphp
                                    @if(!empty(old('preferred_manglik')) && old('preferred_manglik') == $key )
                                        @php $select = "selected"; @endphp
                                    @endif
                                    <option value="{{$key}}" {{$select}}>{{$manglik_type}}</option>
                                @endforeach
                                </select>
                                @error('preferred_manglik')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                </div>
            </div>
        </div>
        
    </div>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary btn-block">Register</button>
        </div>
    </div>
                    </form>
</div>
@endsection
