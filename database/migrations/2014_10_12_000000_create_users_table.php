<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('dob')->nullable();
            $table->integer('gender')->comment("1=>Male","2=>Female");
            $table->string('annual_income');
            $table->string('password');
            $table->integer('occupation')->nullable()->comment("1=>Private job,2=>Government Job,3=>Business");
            $table->integer('family_type')->nullable()->comment("1=>Joint family,2=>Nuclear family");
            $table->integer('manglik')->nullable()->comment("1=>Yes,2=>No");
            $table->string('expected_income');
            $table->integer('preferred_occupation')->comment("1=>Private job,2=>Government Job,3=>Business");
            $table->integer('preferred_family_type')->comment("1=>Joint family,2=>Nuclear family");
            $table->integer('preferred_manglik')->comment("1=>Yes,2=>No");
            $table->integer('user_type')->comment("1=>user,2=>admin");
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
