<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => "_".mt_rand("0","999"),
        'email' => $faker->unique()->email,
        'email_verified_at' => now(),
        'password' => '$2y$10$viNe4DfvZLsWXmPnRoS52.mfMRcXhYE/WOd9pc02/fUjbG2yNbRRa', // 123456
        'remember_token' => Str::random(10),
        'dob' => $faker->randomElement(['1993-07-07','1990-02-25','1989-09-03','1980-03-21']),
        'gender' => $faker->randomElement(['1','2']),
        'annual_income' => $faker->randomElement(['1200000','2500000','1800000','1600000']),
        'occupation' => $faker->randomElement(['1','2','3']),
        'family_type' => $faker->randomElement(['1','2']),
        'manglik' => $faker->randomElement(['1','2']),
        'expected_income' => $faker->randomElement(['1300000','2200000','1900000','1600000']),
        'preferred_occupation' => $faker->randomElement(['1','2','3']),
        'preferred_family_type' => $faker->randomElement(['1','2']),
        'preferred_manglik' => $faker->randomElement(['1','2']),
        'user_type' => '1'
    ];
});
